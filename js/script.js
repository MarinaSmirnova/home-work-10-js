tabsListElements = tabsList.querySelectorAll(".tabs-title");
tabsContentListElements = tabsContentList.querySelectorAll("li");

for (element of tabsListElements) {

    element.addEventListener("click", function() {
        for (const element of tabsListElements) {
            element.classList.remove("active");
        }
        this.classList.toggle("active");
        showContent(this.dataset.title);
    });
}

function showContent(forContent) {
    for (const element of tabsContentListElements) {
        element.style.display = "none";
        if (element.dataset.for === forContent) {
            element.style.display = "block";
        }
    };
};

tabsListElements[0].classList.add("active"); // При відкритті сторінки - тайтл-елемент з індексом 0
showContent(tabsListElements[0].dataset.title); // Текст, що прив'язаний до тайтл-елемента з індексом 0


